# MachineLearningServerServices
This repository create a docker-compose to run on a server with nvidia gpu and nvidiadocker in order to prepare it for machine learining use.

# How to use

## add password
    $ cd security
    $ docker-compose run htpassword
    $ # if you want to add your custom user and pass open htpasswd/docker-compose.yml and change the env variable
    $ # copy the created file in the needed folder (digits_nginix or jupyter)
## create https certificate
    $ cd security
    $ docker-compose run openssl
    $ # copy the created file in the needed folder (digits_nginix or jupyter)
## create jupyter password file
    $ cd security
    $ docker-compose run jupyter_pass
    $ # copy the created file in the needed folder (jupyter)
## run service
    $ cd ..
    $ docker-compose up
    $ # digits
    $ firefox https://localhost:8282
    $ # jupyter
    $ firefox https://localhost:9282 
