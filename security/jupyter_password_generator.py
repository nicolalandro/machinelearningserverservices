from IPython.lib import passwd
import getpass
password_text = getpass.getpass("Insert Password: ")
password_text_repear =  getpass.getpass("Repeat Password: ")
if password_text != password_text_repear:
    print('The passwords is different')
    exit(1)
password = passwd(password_text)
with open("hashed_password","w+") as f:
    f.write(password)
