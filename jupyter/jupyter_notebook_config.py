with open('/etc/jupyter/pass/hashed_password', 'r') as f:
    c.NotebookApp.password = f.read()
c.NotebookApp.certfile = u'/etc/jupyter/ssl/jupyter.crt'
c.NotebookApp.keyfile = u'/etc/jupyter/ssl/jupyter.key'
c.NotebookApp.ip = '*'
c.NotebookApp.open_browser = False
c.NotebookApp.allow_remote_access = True
